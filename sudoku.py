from numpy import NaN as nan
import numpy as np
from copy import deepcopy

class Sudoku():
    def __init__(self,field,depth=0):
        '''
        Pass Sudoku as 9 by 9 numpy array
        '''
        if len(field) == 9:
            if all(len(row) == 9 for row in field):
                self.field = deepcopy(field)
                self.depth = depth
                return

        raise ValueError("Not a valid Sudoku")

    def __str__(self):
        string = ""
        for _ in range(0,self.depth):
            string += '-'
        string += "\n"

        for row in self.field:
            row = [str(el) if not np.isnan(el) else '-' for el in row]
            string += ' '.join(row) + "\n"
        return string

    def remaining_fields(self):
        result = 0
        for row in self.field:
            result += sum(1 if np.isnan(el) else 0 for el in row)
        return result

    @staticmethod
    def remaining_from_num( numbers ):
        is_remaining = lambda x: not x in numbers
        return set(filter( is_remaining, range(1,10) ))

    @staticmethod
    def remove_nan( numbers ):
        is_not_nan = lambda x: not np.isnan(x)
        return list(filter( is_not_nan, numbers ))

    @staticmethod
    def get_row_indices( index ):
        x,_ = index
        return [ (x,i) for i in range(0,9)]

    @staticmethod
    def get_col_indices( index ):
        _,y = index
        return [ (i,y) for i in range(0,9)]

    @staticmethod
    def get_square_indices( index ):
        x,y = index

        x_range = list(range(int(x / 3) * 3, int(x / 3) * 3 + 3))
        y_range = list(range(int(y / 3) * 3, int(y / 3) * 3 + 3))

        return [(x,y) for x in x_range for y in y_range]

    def get_from_indices(self, indices):
        return [ self.field[x][y] for x,y in indices ]

    def get_row_filled(self, index):
        row_index = self.get_row_indices( index )
        return self.remove_nan(self.get_from_indices(row_index))

    def get_col_filled(self, index):
        col_index = self.get_col_indices( index )
        return self.remove_nan(self.get_from_indices(col_index))

    def get_square_filled(self, index):
        indices = self.get_square_indices(index)
        return self.remove_nan(self.get_from_indices(indices))

    def check_index(self, index):
        x,y = index
        if x > 8 or y > 8:
            raise ValueError("Index not Valid")

        if not np.isnan(self.field[x][y]):
            return

        filled = []

        filled += self.get_row_filled(index)
        filled += self.get_col_filled(index)
        filled += self.get_square_filled(index)

        remaining = self.remaining_from_num(filled)

        self.remaining_dict[index] = remaining

        if len(remaining) == 1:
            self.field[x][y] = list(remaining)[0]

    def process_dict(self,own_index, indices):
        x,y = own_index
        if x > 8 or y > 8:
            raise ValueError("Index not Valid")

        if not np.isnan(self.field[x][y]):
            return

        indices = [ index for index in indices if index != own_index ]

        own_possible_numbers = [el for el in self.remaining_dict[own_index]]
        possible_numbers = list(self.remaining_dict[index] for index in indices if index in self.remaining_dict)
        possible_numbers = list(set(item for sublist in possible_numbers for item in sublist))

        own_possible_numbers = [number for number in own_possible_numbers if not number in possible_numbers ]
        if len(own_possible_numbers) == 1:
            self.field[x][y] = own_possible_numbers[0]

    def check_dictionary(self, own_index):
        self.process_dict(own_index,self.get_square_indices(own_index))
        self.process_dict(own_index,self.get_row_indices(own_index))
        self.process_dict(own_index,self.get_col_filled(own_index))

    def solve_iteration(self,verbose=False):

        self.remaining_dict = {}

        indices = [(x,y) for x in range(0,9) for y in range(0,9)]
        for index in indices:
            self.check_index(index)

        for index in indices:
            self.check_dictionary(index)

        if verbose:
            print(self.remaining_dict)

    def solve_recursive(self):

        filtered = [ (index,changes) for index,changes in self.remaining_dict.items()
                                     if len(changes) != 0 ]

        try:
            index, changes = filtered[0]
        except IndexError:
            return False

        for change in reversed(list(changes)):

            x,y = index
            new_depth = self.depth + 1
            new_sudoku = Sudoku(self.field)
            new_sudoku.field[x][y] = change

            if new_sudoku.solve():
                return True

        return False

    def solve(self):
        for _ in range(0,81):
            self.solve_iteration()

        if self.remaining_fields() > 0:
            return self.solve_recursive()
        else:
            print(self)
            return True
