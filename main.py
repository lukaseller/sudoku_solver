from sudoku import Sudoku
import numpy as np
from numpy import NaN as nan

leicht1 = [
    [3,nan,6,nan,5,2,nan,1,7],
    [5,2,4,nan,nan,1,nan,nan,6],
    [1,8,7,6,nan,9,nan,4,2],
    [nan,5,3,2,1,nan,nan,nan,9],
    [8,1,nan,3,4,6,2,7,nan],
    [6,4,nan,nan,nan,5,nan,3,nan],
    [9,nan,nan,1,2,nan,6,nan,3],
    [2,6,nan,nan,9,3,7,8,4],
    [4,nan,nan,8,6,nan,nan,2,1],
]

schwer1 = [
    [4,nan,nan,3,1,9,nan,nan,nan],
    [nan,2,nan,8,nan,nan,nan,nan,nan],
    [9,nan,1,nan,4,nan,nan,nan,8],
    [nan,5,nan,nan,8,nan,7,nan,nan],
    [nan,nan,2,nan,6,nan,nan,5,nan],
    [7,9,8,nan,nan,nan,6,1,nan],
    [3,6,nan,nan,nan,nan,nan,nan,1],
    [2,nan,nan,nan,nan,nan,nan,nan,nan],
    [nan,1,nan,nan,nan,nan,9,2,nan],
]

schwer2 = [
    [nan,nan,nan,9,nan,nan,nan,nan,nan],
    [nan,nan,nan,nan,nan,5,nan,nan,2],
    [nan,nan,8,nan,7,nan,nan,nan,nan],
    [4,nan,nan,nan,nan,nan,2,nan,nan],
    [1,6,5,2,nan,3,9,nan,nan],
    [9,nan,3,nan,6,nan,nan,4,nan],
    [8,nan,nan,1,nan,7,nan,3,4],
    [6,nan,nan,nan,nan,nan,8,nan,nan],
    [nan,nan,2,3,8,nan,nan,1,9],
]

schwer3 = [
    [7,nan,nan,nan,nan,nan,nan,nan,4],
    [nan,5,nan,nan,nan,nan,nan,nan,nan],
    [nan,8,nan,9,7,nan,5,nan,nan],
    [nan,nan,nan,nan,nan,nan,9,nan,nan],
    [5,nan,9,nan,1,7,nan,nan,3],
    [nan,nan,nan,nan,4,nan,nan,6,5],
    [4,nan,nan,8,nan,nan,nan,nan,9],
    [6,nan,nan,7,2,nan,nan,nan,8],
    [1,2,nan,nan,nan,nan,4,3,nan],
]

sudokus = [leicht1, schwer1, schwer2, schwer3]

for sudoku in sudokus:
    Sudoku(sudoku).solve()
